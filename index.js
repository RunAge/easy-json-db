const fs = require('fs');
module.exports = {
  DB: {},
  load(file) {
    try {
      this.DB = JSON.parse(fs.readFileSync(file));
    }
    catch(err){
      throw `${err}\nAdd "{}" in file if file is empty.`;
    }
    return this;
  },
  drop() {
    this.DB = {};
    return this;
  },
  set(id, object) {
    this.DB[id] = object;
    return this;
  },
  remove(id) {
    delete this.DB[id];
    return this;
  },
  save() {
    const json = JSON.stringify(this.DB, undefined, 2);
    fs.writeFileSync('db.json', json);
    return this;
  }
}